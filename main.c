/******************************************************************************
* File Name :   main.c
* Author :      Клиначев Николай Васильевич
* Version :     1.0.3
* Date :        20150618
* Target :      ARM Cortex-M4 (STM32F303xC / STM32F3DISCOVERY), FPU:Off
* Description : Программа векторной системы управления для СДПМ (PMSM / BLAC)
*               Неполная версия (скелет программы, ШИМ-драйвер)
*       Реализован код ШИМ-драйвера для питания секций статора 3-х фазного
*       двигателя. Счетчик таймера считает тактирующие импульсы в прямом и
*       в обратном направлении, формируя опорный сигнал треугольной формы.
*       Регистры сравнения 3-х каналов таймера формируют сигналы для
*       управления 6-тью ключами 3-х стоек силового моста с бестоковой паузой.
*       Код формирования сигнала для ШИ-модуляции реализован в процедуре
*       обработки прерывания таймера и вызывается при максимальном и при
*       нулевом кодах в счетчике. Для выборки и преобразования табличных
*       значений синусоиды реализованы: интегратор угла, инверсные
*       преобразователи Парка и Кларка. Интегратор позволяет контролировать
*       частоту, а преобразователи амплитуду сигнала для ШИ-модулятора.
*       Для вычислений используется целочисленные операции и макросы
*       IQ-математики.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"  // Подключаем библиотеки производителя процессора
#include "mclib.h"      // Подключаем библиотеки IQ-Math и Motor Control

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SYSCLK          72000000                // SystemCoreClock
#define PWM_COUNTER     2048                    // 2048 == 17578.125 Гц
#define PWM_FREQUENCY   (0.5 * SYSCLK / PWM_COUNTER) // div2 4 TRANGLE
#define PWM_RESOLUTION  PWM_COUNTER             // Разрешение падает с ростом частоты
#define TIMESTEP        (1.0 / PWM_FREQUENCY)   // Шаг дискретизации ИУ, Ротаторов (сек)
#define BASE_FREQ       200.0                   // Максимальная частота фазных токов (Hz)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

typedef struct {
    _iq gS;    // Output: Уставка Скорости, [об/мин/BASE_SPD_RPM], [-1.0, +1.0], (pu)
    _iq GdV;   // Output: d-Катет управляющего значения для ШИМ, [-1.0, 1.0), (pu)
    _iq GqV;   // Output: q-Катет управляющего значения для ШИМ, [-1.0, 1.0), (pu)
    FunctionalState SVPWM_Enable; // Output: флаг ативизации SVPWM
} SETPOINTS_TypeDef;
// Глобальные уставки Цифровой системы управления для СДПМ (PMSM / BLAC)
SETPOINTS_TypeDef SetPnt = { _IQ(0.15), _IQ(0.0), _IQ(0.9), ENABLE };
// Интегратор Угла и другие координаты, связанные с положением ротора
ROTOR_TypeDef           Rotor;
// Инверсный преобразователь Парка (dq2ab == dc2ac)
PIPARK_TypeDef          dq2ab;
// Инвертирующий преобразователь Кларка (преобразователь числа фаз - 2Ph_2_3Ph)
PICLARKE_TypeDef        ab2uvw;

/* Private function prototypes -----------------------------------------------*/
static void RCC_Configuration(void);
static void GPIO_Configuration(void);
static void NVIC_Configuration(void);
static void TIM_Configuration(void);
static void PMSM_CntrlUnit_CreateWires(void);
void TIM1_UP_TIM16_IRQHandler(void);

/*******************************************************************************
* Function Name : main
* Description :   Main program
*******************************************************************************/
#pragma optimize=none
int main(void) {
    // На этой стадии тактовый генератор процессора уже сконфигурирован,
    // это сделано функцией SystemInit(), которая была вызвана в "startup-файле"
    // (startup_stm32f30x.s), прежде чем была вызвана главная функция приложения
    // main. Для реконфигурирования значений по умолчанию обратитесь к файлу
    // system_stm32f30x.c, который создаётся изготовителем процессора.

    // Настраиваем тактовый генератор процессора
    RCC_Configuration();
    // Заполняем таблицу векторов прерываний
    NVIC_Configuration();
    // Конфигурируем порты ввода / вывода
    GPIO_Configuration();
    // Конфигурируем таймер
    TIM_Configuration();

    PMSM_CntrlUnit_CreateWires();

    while (1) {}
}

/*******************************************************************************
* Function Name : RCC_Configuration
* Description :   Reset and Clock Control.
*******************************************************************************/
static void RCC_Configuration(void) {
#ifdef NONE_SYSTEM_STM32F30X_C
    // Сбрасываем состояние системного генератора к начальному
    RCC_DeInit();       // RCC system reset (for debug purpose)
    // Включаем внешний высокочастотный кварцевый генератор
    RCC_HSEConfig(RCC_HSE_ON);

    // Ожидаем выхода генератора на рабочий режим
    if (RCC_WaitForHSEStartUp() == !SUCCESS) {
        while (1) {} // неисправен тактовый генератор
    }
    // Активируем буфер упреждающей выборки для флеш-памяти
    FLASH_PrefetchBufferCmd(ENABLE);
    // Определим задержку для Флеш-памяти
    FLASH_SetLatency(FLASH_Latency_2);

    // Настроим тактовый генератор процессора,
    //      см. док: DM00043574.pdf, стр. 127, Figure 13
    // Конфигурируем делитель для: ядра, памяти, AHB-шины, ПДП
    RCC_HCLKConfig(RCC_SYSCLK_Div1);        // HCLK = SYSCLK
    // Конфигурируем делитель для высокоскоростной периферии
    RCC_PCLK2Config(RCC_HCLK_Div1);         // PCLK2 = HCLK
    // Конфигурируем делитель для низкоскоростной периферии
    RCC_PCLK1Config(RCC_HCLK_Div2);         // PCLK1 = HCLK/2

    // Установим коэффициент умножения частоты кварца
    // для системного генератора PLLCLK = 8MHz * 9 = 72 MHz
    RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);
    // Включаем контур ФАПЧ (PLL) системного генератора
    RCC_PLLCmd(ENABLE);
    // Ожидаем завершения переходного процесса в контуре ФАПЧ
    while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {}
    // Выбираем контур ФАПЧ в качестве системного генератора
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); // SYSCLK = PLLCLK
    // Ожидаем выхода на рабочий режим всех делителей
    while (RCC_GetSYSCLKSource() != 0x08) {}
#endif

    // Настроим делитель частоты системного генератора для АЦП12
    RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div1); // ADCCLK = PLLCLK

    // Включаем тактирование устройств на системной AHB-шине с высокой
    // пропускной способностью (Advanced High-performance Bus)
    RCC_AHBPeriphClockCmd(
        RCC_AHBPeriph_GPIOA |   // порт A
        RCC_AHBPeriph_GPIOB,    // порт B
        ENABLE);
    // Включаем тактирование устройств на APB2-шине
    // высокоскоростной периферии: TIM1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
}

/*******************************************************************************
* Function Name : NVIC_Configuration
* Description : Определяем таблицу векторов прерываний и настраиваем приоритеты
*******************************************************************************/
static void NVIC_Configuration(void) {
    NVIC_InitTypeDef NVIC_InitStructure;

#ifdef NONE_SYSTEM_STM32F30X_C
#ifdef VECT_TAB_SRAM
    // Разместим таблицу векторов прерываний в ОЗУ: 0x20000000
    NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0);
#else
    // Разместим таблицу векторов прерываний в ПЗУ: 0x08000000
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
#endif
#endif

    // Определим, как прерывания будут сгруппированы по приоритетам
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    // Определим вектор прерывания для Таймера (TIM1, TIM_IT_Update)
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TIM16_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name : GPIO_Configuration
* Description : Configure the TIM1 Pins.
*******************************************************************************/
static void GPIO_Configuration(void) {
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    // Порт B: Терминал 11 для контроля осциллографом при отладке
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    // Порт A: Терминалы 8, 9, 10 конфигурируем для вывода PWM
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    // Порт B: Терминалы 13, 14, 15 конфигурируем для вывода PWM
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    // Подключаем терминалы к альтернативным источникам сигнала (TIM1_PWM)
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_6);  // TIM1_CH1
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_6);  // TIM1_CH2
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_6); // TIM1_CH3
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_6); // TIM1_CH1N
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_6); // TIM1_CH2N
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_4); // TIM1_CH3N
}

/*******************************************************************************
* Function Name : TIM_Configuration
* Description :   Конфигурируем тоймер TIM1
*     Генерируем 3 пары комплементарных ШИМ-сигналов с бестоковой паузой (для
*     контроля состояния ключей стоек полупроводникового 3-х фазного моста).
*     При изменении частоты ШИМ-драйвера меняется максимальное значение,
*     до которого таймер считает тактирующие импульсы. Уставки для регистров
*     сравнения должны учитывать разрешение ШИ-модулятора (должны быть меньше).
*     Для их масштабирования предложен макрос _Q15toBASE, которому, в качестве
*     аргументов, передается выборка и параметр PWM_RESOLUTION.
*******************************************************************************/
static void TIM_Configuration(void) {
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef       TIM_OCInitStructure;
    TIM_BDTRInitTypeDef     TIM_BDTRInitStructure;
    // Настроим тактирование и счетчик таймера
    //
    // 1. Настроим двоичный делитель тактирующей частоты
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    // 2. Настроим поглощающий счетчик счетных импульсов
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    // 3. Активируем режим реверсивного счёта (inc/dec)
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1;
    // 4. Установим максимальное число для счёта
    TIM_TimeBaseStructure.TIM_Period = PWM_COUNTER - 1;
    // 5. Настроим поглощающий прерывания счетчик реверсов
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 2 - 1;
    // и как ... сконфигурируем!
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

    // Настроим выходы каналов Захвата / Сравнения таймера
    //
    // 1. Установим режим сравнения CCR >= CNT или CCR <= CNT
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    // 2. Активируем выход OCx Регистра Сравнения (верхний ключ)
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    // 3. Активируем выход OCNx Регистра Сравнения (нижний ключ)
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
    // 4. Установим полярность сигнала OCx для верхнего ключа
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    // 5. Установим полярность сигнала OCNx для нижнего ключа
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
    // 6. Определим состояние выходов OCx  для IDLE режима
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    // 7. Определим состояние выходов OCNx для IDLE режима
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
    // Установим число в Регистра Сравнения = половину от ARR
    TIM_OCInitStructure.TIM_Pulse = (PWM_RESOLUTION >> 1) - 1;
    // TIM1: Конфигурируем каналы: 1, 2, 3 (заполение 50%)
    TIM_OC1Init(TIM1, &TIM_OCInitStructure);
    TIM_OC2Init(TIM1, &TIM_OCInitStructure);
    TIM_OC3Init(TIM1, &TIM_OCInitStructure);

    // Настроим защиту моста (ST.com: DM00080497.pdf DM00042534.pdf)
    //
    // 1. Определим величину бестоковой паузы
    TIM_BDTRInitStructure.TIM_DeadTime = 54; // 54 / 72e6 = 0.75 uS
    // 2. [Де]Активируем Компаратор Защиты
    TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;
    // 3. Укажем активный уровень для Компаратора Защиты
    TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
    // 4. Установим уровень срабатывания Компаратора Защиты
    TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_1;
    // 5. Не используем однотактный режим управления мостом (Run-режим)
    TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
    // 6. Не отключаем таймер от выходов при выключении ШИМ-а (Idle-режим)
    TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
    // 7. Не используем автоматическое включение после срабатывания защиты
    TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Disable;
    // и как ... сконфигурируем!
    TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);

    // TIM1: Сбрасываем флаг прерывания по обновлению
    TIM_ClearFlag(TIM1, TIM_FLAG_Update);
    // TIM1: Разрешаем прерывание по обновлению
    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
    // TIM1: Включаем счетчик таймера
    TIM_Cmd(TIM1, ENABLE);
    // TIM1: Включаем главный ШИМ-выход таймера
    TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

/*******************************************************************************
* Function Name : PMSM_CntrlUnit_CreateWires
* Description :   ...
*     Процедура выполняет построение цифровой системы управления для
*     СДПМ (PMSM) из вычислительных модулей - экземпляров объектов.
*     Суть построения ЦСУ - определение схемы передачи аргументов.
*     Выход - это атрибут модуля (переменная принадлежит объекту).
*     Вход - не является атрибутом модуля (объект имеет указатели
*     для подключения к аргументам).
*******************************************************************************/
static void PMSM_CntrlUnit_CreateWires(void) {
    // Интегратор Угла (Вычислитель Углового положения ротора)
    Rotor.yS     = &SetPnt.gS;      // Подключили модуль к Наблюдателю Скорости вала
    // Инверсный преобразователь Парка
    dq2ab.d      = &SetPnt.GdV;     // Подключили выходной сигнал РТ для d-оси
    dq2ab.q      = &SetPnt.GqV;     // Подключили выходной сигнал РТ для q-оси
    dq2ab.Sine   = &Rotor.Sine;     // Подключили ротатор к опорной синусоиде
    dq2ab.Cosine = &Rotor.Cosine;   // Подключили ротатор к опорной косинусоиде
    // Преобразователь числа фаз
    ab2uvw.a     = &dq2ab.a;        // Подключили Alpha-фазу к преобразователю
    ab2uvw.b     = &dq2ab.b;        // Подключили  Beta-фазу к преобразователю
}

/*******************************************************************************
* Function Name : TIM1_UP_TIM16_IRQHandler 4 TIM_IT_Update
* Description :   Самая главная процедура программы (обработка прерывания)
*     TIM_IT_Update на периоде треугольного ШИМ-а может генерироваться дважды.
*     Перед нулевым и перед максимальным кодами в счетчике таймера.
*     Но поглощающий прерывания счетчик реверсов может их прореживать.
*     Что влияет на шаг дискретизации системы управления (на TIMESTEP).
*******************************************************************************/
//#pragma optimize=speed
void TIM1_UP_TIM16_IRQHandler(void) {

    //if (TIM_GetITStatus(TIM1, TIM_IT_Update) == RESET) return;
    GPIOB->ODR ^= GPIO_Pin_11;
    // 0.18 uS

    // ------------------------------------------------------------------------------
    //  Наблюдатель углового положения ротора
    // ------------------------------------------------------------------------------
    // Angle = Speed * (100 * PI) * (POLES/2) * TIMESTEP * 1/s, [0.0, 2*PI]
    // где: Speed - в относительных единицах (pu) - [-1.0, +1.0]
    //      Angle - в абсолютных единицах (рад)   - [0.0,  2*PI]
    // ИЛИ
    // Angle = Speed * (100 / 2) * (POLES/2) * TIMESTEP * 1/s =
    //       = Speed *       BASE_FREQ       * TIMESTEP * 1/s, [0.0, 1.0]
    // где: Speed - в относительных единицах (pu) - [-1.0, +1.0]
    //      Angle - в относительных единицах (pu) - [ 0.0,  1.0]
    // ------------------------------------------------------------
    Rotor.Angle += _IQmpy(*Rotor.yS, _IQ(BASE_FREQ * TIMESTEP)); // 0.8 uS
    if (Rotor.Angle > _IQ(1.0)) Rotor.Angle -= _IQ(1.0);
    if (Rotor.Angle < _IQ(0.0)) Rotor.Angle += _IQ(1.0);
    // Внимание! Перегрузка: _IQtoIQ15(_IQ(1.0)) === 0x8000
    ROTOR_GetTrigonometic(&Rotor);
    // 2.4 uS

    // ------------------------------------------------------------------------------
    //  Преобразуем управляющие сигналы Регуляторов тока в 2-x фазную систему напряжений
    // ------------------------------------------------------------------------------
    dq2ab.a = _IQmpy(*dq2ab.d, *dq2ab.Cosine) - _IQmpy(*dq2ab.q, *dq2ab.Sine);
    dq2ab.b = _IQmpy(*dq2ab.q, *dq2ab.Cosine) + _IQmpy(*dq2ab.d, *dq2ab.Sine);
    // 3.8 uS

    // ------------------------------------------------------------------------------
    //  Преобразуем 2-x фазную систему напряжений в 3-x фазную (для питания электромотора)
    // ------------------------------------------------------------------------------
    _iq temp_v1 = _IQdiv2(*ab2uvw.a); // 0.8660254037844386 = sqrt(3)/2
    _iq temp_v2 = _IQmpy(*ab2uvw.b, _IQ(0.8660254037844386));
    ab2uvw.u = -(*ab2uvw.a);
    ab2uvw.v = temp_v1 - temp_v2;
    ab2uvw.w = temp_v1 + temp_v2;
    // 4.7 uS

    // ------------------------------------------------------------------------------
    //  Преобразуем 3-x фазную синусоидальную последовательность в ... и будет SVPWM
    // ------------------------------------------------------------------------------
    if (SetPnt.SVPWM_Enable) {
        if (ab2uvw.u < ab2uvw.v) {
            if (ab2uvw.u < ab2uvw.w) {
                ab2uvw.v = ab2uvw.v - (_IQ(1.0) + ab2uvw.u);
                ab2uvw.w = ab2uvw.w - (_IQ(1.0) + ab2uvw.u);
                ab2uvw.u = _IQ(-1.0);
            } else {
                ab2uvw.u = ab2uvw.u - (_IQ(1.0) + ab2uvw.w);
                ab2uvw.v = ab2uvw.v - (_IQ(1.0) + ab2uvw.w);
                ab2uvw.w = _IQ(-1.0);
            }
        } else if (ab2uvw.v < ab2uvw.w) {
            ab2uvw.w = ab2uvw.w - (_IQ(1.0) + ab2uvw.v);
            ab2uvw.u = ab2uvw.u - (_IQ(1.0) + ab2uvw.v);
            ab2uvw.v = _IQ(-1.0);
        } else if (ab2uvw.w < ab2uvw.v) {
            ab2uvw.u = ab2uvw.u - (_IQ(1.0) + ab2uvw.w);
            ab2uvw.v = ab2uvw.v - (_IQ(1.0) + ab2uvw.w);
            ab2uvw.w = _IQ(-1.0);
        } else {
            ab2uvw.u = _IQ(-1.0);
            ab2uvw.v = _IQ(-1.0);
            ab2uvw.w = _IQ(-1.0);
        }
    }
    // SVM: +1.9 .. 2.4 uS = 6.6 .. 7.0 uS

    // ------------------------------------------------------------------------------
    //  Ограничиваем сигналы (чтоб не было перегрузки в макросе _IQtoIQ15)
    // ------------------------------------------------------------------------------
    if (ab2uvw.u > _IQ(0.99999)) ab2uvw.u = _IQ(0.99999);
    if (ab2uvw.u < _IQ(-1.0000)) ab2uvw.u = _IQ(-1.0000);
    if (ab2uvw.v > _IQ(0.99999)) ab2uvw.v = _IQ(0.99999);
    if (ab2uvw.v < _IQ(-1.0000)) ab2uvw.v = _IQ(-1.0000);
    if (ab2uvw.w > _IQ(0.99999)) ab2uvw.w = _IQ(0.99999);
    if (ab2uvw.w < _IQ(-1.0000)) ab2uvw.w = _IQ(-1.0000);
    // +1.5 uS | SVM: +1.6 .. 1.82 uS | PWM: +1.68 .. 1.86 uS
    // ------------------------------------------------------------------------------
    //  Обновляем Регистры Сравнения 3-х каналов Таймера
    // ------------------------------------------------------------------------------
    TIM_SetCompare1(TIM1, _Q15toBASE(_IQtoIQ15(ab2uvw.u), PWM_RESOLUTION));
    TIM_SetCompare2(TIM1, _Q15toBASE(_IQtoIQ15(ab2uvw.v), PWM_RESOLUTION));
    TIM_SetCompare3(TIM1, _Q15toBASE(_IQtoIQ15(ab2uvw.w), PWM_RESOLUTION));
    TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
    // PWM: 8.2 uS | SVM: 9.6 .. 10.1 uS
    GPIOB->ODR ^= GPIO_Pin_11;
}

#ifdef USE_FULL_ASSERT
/*******************************************************************************
* Function Name :       assert_failed
* Description :         Отчеты об имени исходного файла и о номере строки,
*                       где в режиме отладки произошла assert_param ошибка.
* Input : - file :      Указатель на имя исходного файла
*         - line :      Номер строки кода с ошибкой assert_param
* Output :              None
* Return :              None
*******************************************************************************/
void assert_failed(u8 * file, u32 line) {
    // Пользователь может определить собственное сообщение об имени файла
    // и номере строки.
    // Пример:
    // printf("Ошибка величины параметра: file %s on line %d\r\n", file, line);
    while (1) {}
}
#endif

/************************ END OF FILE *****************************************/